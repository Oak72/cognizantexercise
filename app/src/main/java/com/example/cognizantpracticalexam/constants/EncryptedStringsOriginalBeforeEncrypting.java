package com.example.cognizantpracticalexam.constants;

import androidx.annotation.NonNull;

import com.example.cognizantpracticalexam.tools.StringEncryptor;

/**
 * This file is meant to show how this enum worked with non-encrypted values.
 * Don't consider this as part of project code, but rather as a demonstration
 */
public enum EncryptedStringsOriginalBeforeEncrypting {

    // Constants
    JSON_KEY_NAME("name"),
    JSON_KEY_FIELDS("fields"),
    JSON_KEY_TYPE("type"),
    JSON_KEY_DATA("data"),
    SELECTION_QUERY(" = ?"),
    REQUEST_PERMISSION_PERMISSION_STRING("permission"),
    REQUEST_PERMISSION_REQUESTCODE_STRING("requestCode"),
    UPLOAD_URL("https://thiswebsitedoesnotexist.fake"),
    JSON_MEDIA_TYPE_STRING("application/json; charset=utf-8"),
    INTENT_UPLOAD_CONTACTS_ARRAY("contactsArray"),

    ACTIVITY_PERMISSION_ACTION("com.example.cognizantpracticalexam.PERMISSION_REQUEST"),
    ACTIVITY_PERMISSION_GRANTRESULTS_STRING("grantResults"),
    ACTIVITY_MESSAGE("This is totally not malware!");

    //marked as public just for tests, this should be private
    public final String encryptedValue;

    EncryptedStringsOriginalBeforeEncrypting(String encryptedString) {
        this.encryptedValue = encryptedString;
    }


    @NonNull
    @Override
    public String toString() {
        return StringEncryptor.decrypt(encryptedValue);
    }
}