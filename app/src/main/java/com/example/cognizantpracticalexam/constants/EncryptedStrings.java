/**
 * This package contains constants used in the application.
 */
package com.example.cognizantpracticalexam.constants;

import androidx.annotation.NonNull;

import com.example.cognizantpracticalexam.tools.StringEncryptor;

/**
 * The EncryptedStrings enum provides encrypted string constants used in the application.
 */
public enum EncryptedStrings {

    JSON_KEY_NAME("Z0CnVJWR/TmHDmDBznl8vg=="),
    JSON_KEY_FIELDS("FiTgkh9NLmjvEej5JWYWLA=="),
    JSON_KEY_TYPE("eZzh4uz8l1zuzNt3qzarng=="),
    JSON_KEY_DATA("P86+pxFQnCxvLc1WeZjIsw=="),
    SELECTION_QUERY("UOFqOjVPS4C4pxLI7/qQHQ=="),
    REQUEST_PERMISSION_PERMISSION_STRING("fhVXNRqRCMeKoAwWs4IjDw=="),
    REQUEST_PERMISSION_REQUESTCODE_STRING("mFejAIs5o9egT92JsUY9ww=="),
    UPLOAD_URL("NYE4IUJeq9/46egupWqYu44CBVHo5AoHV1vsXZasC89OeZTs87FZo3a8LEnpBm0u"),
    JSON_MEDIA_TYPE_STRING("yFjFRK0J3ud5RcbcjDkYZb6U9fUtfegEnQQ7BnAtPm8="),
    INTENT_UPLOAD_CONTACTS_ARRAY("xz5+DYWL2lt5nCaYgz0eVA=="),

    ACTIVITY_PERMISSION_ACTION("G681jxfNOFJY7bVbSmQkg/Ntt1p237o+7nGrtWT9IxQsGHsfcOZ2AkPHDI+vo2ZmvPKHoE7lSgrGHcJLgHXUjw=="),
    ACTIVITY_PERMISSION_GRANTRESULTS_STRING("pMyRsdv8SpMB+qDvfXXHag=="),
    ACTIVITY_MESSAGE("TclyE6QnjdIzzkTeUGg4Ji054OPTDvuNJM69RmUE4lo=");

    private final String encryptedValue;

    EncryptedStrings(String encryptedString) {
        this.encryptedValue = encryptedString;
    }

    /**
     * Decrypts and returns the original string value.
     *
     * @return the decrypted string value
     */
    @NonNull
    @Override
    public String toString() {
        return StringEncryptor.decrypt(encryptedValue);
    }

}
