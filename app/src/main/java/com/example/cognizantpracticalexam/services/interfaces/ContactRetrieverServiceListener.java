/**
 * This package contains interfaces related to the contact retriever service.
 */
package com.example.cognizantpracticalexam.services.interfaces;

import org.json.JSONArray;

/**
 * The ContactRetrieverServiceListener interface provides callbacks for events related to contact retrieval.
 */
public interface ContactRetrieverServiceListener {

    /**
     * Called when contacts are successfully retrieved.
     *
     * @param contactsArray the JSONArray containing the retrieved contacts
     */
    void onContactsRetrieved(JSONArray contactsArray);

    /**
     * Called when the permission to retrieve contacts is granted.
     */
    void onPermissionGranted();

    /**
     * Called when the permission to retrieve contacts is denied.
     */
    void onPermissionDenied();
}
