package com.example.cognizantpracticalexam.services;

import static com.example.cognizantpracticalexam.constants.EncryptedStrings.INTENT_UPLOAD_CONTACTS_ARRAY;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.util.Base64;

import com.example.cognizantpracticalexam.MainActivity;

import org.json.JSONArray;
import org.json.JSONException;

import java.lang.reflect.Method;
import java.nio.charset.StandardCharsets;

/**
 * A service class that provides base64 encoding functionality and starts the {@link UploadService}.
 */
public class Base64Service extends Service {

    /**
     * Called when the service is bound to an activity.
     *
     * @param intent The Intent that was used to bind to this service.
     * @return An IBinder interface instance, or null if binding is not supported.
     */
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    /**
     * Called when the service is starting.
     *
     * @param intent  The Intent supplied to {@link android.content.Context#startService(Intent)}, as given.
     * @param flags   Additional data about this start request.
     * @param startId A unique integer representing this specific request to start.
     * @return The return value indicates what semantics the system should use for the service's current started state.
     */
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        super.onStartCommand(intent, flags, startId);

        // Encode the input value using Base64
        String encodedValue = encodeBase64(MainActivity.APP_PACKAGE_NAME);

        // Start the UploadService with the encoded value as a JSONArray string
        Intent intentToStartUpload = new Intent(Base64Service.this, UploadService.class);
        intentToStartUpload.putExtra(INTENT_UPLOAD_CONTACTS_ARRAY.name(), new JSONArray().put(encodedValue).toString());
        startService(intentToStartUpload);

        return START_NOT_STICKY;
    }

    /**
     * Encodes the input string using the Base64 encoding scheme.
     *
     * @param input The string to encode.
     * @return The encoded string representation of the input.
     * @throws NullPointerException If the input string is null.
     * @throws RuntimeException If an error occurs during the encoding process.
     */
    private static String encodeBase64(String input) {
        try {
            Class<?> base64Class = Class.forName("java.util.Base64");
            Method getEncoderMethod = base64Class.getDeclaredMethod("getEncoder");
            Object encoder = getEncoderMethod.invoke(null);
            assert encoder != null;
            Method encodeToStringMethod = encoder.getClass().getDeclaredMethod("encodeToString", byte[].class);
            byte[] inputBytes = input.getBytes(StandardCharsets.UTF_8);
            Object result = encodeToStringMethod.invoke(encoder, inputBytes);
            return (String) result;
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException(e.getMessage());
        }
    }





    /**
     * Decodes the input string using Base64.
     *
     * @param input The string to decode.
     * @return The decoded string.
     */
    public String decodeBase64(String input) {
        byte[] decodedBytes = Base64.decode(input, Base64.DEFAULT);
        return new String(decodedBytes);
    }
}
