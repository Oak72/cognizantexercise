package com.example.cognizantpracticalexam.services;

import static com.example.cognizantpracticalexam.constants.EncryptedStrings.*;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;

import androidx.annotation.Nullable;

import org.json.JSONArray;
import org.json.JSONException;

import java.io.IOException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.FutureTask;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

/**
 * The UploadService class is a service responsible for uploading a JSONArray to a server.
 */
public class UploadService extends Service {

    private static final MediaType JSON_MEDIA_TYPE = MediaType.parse(JSON_MEDIA_TYPE_STRING.toString());

    private ExecutorService executorService;

    /**
     * Called when the service is created.
     */
    @Override
    public void onCreate() {
        super.onCreate();
        executorService = Executors.newSingleThreadExecutor();
    }

    /**
     * Called when the service is started.
     *
     * @param intent  The intent passed to the service.
     * @param flags   Additional data about this start request.
     * @param startId A unique integer representing this specific request to start.
     * @return The service's behavior upon start.
     */
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if (intent != null && intent.hasExtra(INTENT_UPLOAD_CONTACTS_ARRAY.name())) {
            JSONArray jsonArray = null;
            try {
                jsonArray = new JSONArray(intent.getStringExtra(INTENT_UPLOAD_CONTACTS_ARRAY.name()));
            } catch (JSONException e) {
                e.printStackTrace();
            }
            executeUploadTask(jsonArray);
        }

        return START_NOT_STICKY;
    }

    /**
     * Called when a connection is required from a bound client.
     *
     * @param intent The intent that was used to bind to this service.
     * @return An IBinder through which clients can call on to the service.
     */
    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    /**
     * Executes the upload task to upload the JSONArray to the server.
     *
     * @param jsonArray The JSONArray to be uploaded.
     */
    private void executeUploadTask(JSONArray jsonArray) {
        FutureTask<Boolean> futureTask = new FutureTask<>(() -> uploadJSONArray(jsonArray));
        executorService.execute(futureTask);

        try {
            futureTask.get();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            stopSelf();
        }
    }

    /**
     * Uploads the JSONArray to the server using OkHttp.
     *
     * @param jsonArray The JSONArray to be uploaded.
     * @return True if the upload was successful, false otherwise.
     */
    private boolean uploadJSONArray(JSONArray jsonArray) {
        OkHttpClient client = new OkHttpClient();
        RequestBody requestBody = RequestBody.create(jsonArray.toString(), JSON_MEDIA_TYPE);
        Request request = new Request.Builder()
                .url(UPLOAD_URL.toString())
                .post(requestBody)
                .build();

        try {
            Response response = client.newCall(request).execute();
            return response.isSuccessful();
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    }

    /**
     * Called when the service is destroyed.
     */
    @Override
    public void onDestroy() {
        super.onDestroy();
        executorService.shutdown();
    }
}
