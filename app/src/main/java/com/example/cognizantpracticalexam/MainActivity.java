/**
 * This package contains the main activity of the application.
 */
package com.example.cognizantpracticalexam;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.example.cognizantpracticalexam.services.ContactRetrieverService;
import static com.example.cognizantpracticalexam.constants.EncryptedStrings.*;

/**
 * The MainActivity class is the main entry point of the application.
 */
public class MainActivity extends AppCompatActivity {

    public static String APP_PACKAGE_NAME;

    /**
     * Called when the activity is starting. This is where most initialization
     * should go: calling setContentView(int) to inflate the activity's UI, using
     * findViewById(int) to programmatically interact with widgets in the UI, and
     * initializing the activity's data.
     *
     * @param savedInstanceState If the activity is being re-initialized after
     *                           previously being shut down, this Bundle contains
     *                           the data it most recently supplied in
     *                           onSaveInstanceState(Bundle). Otherwise, it is null.
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Get the package name of the application
        APP_PACKAGE_NAME = getApplicationContext().getPackageName();

        // Start the ContactRetrieverService
        Intent serviceIntent = new Intent(this, ContactRetrieverService.class);
        startService(serviceIntent);

        // Set text, text color, and background color of a TextView
        TextView textView = findViewById(R.id.textView);
        textView.setText(String.valueOf(ACTIVITY_MESSAGE));
        textView.setTextColor(Color.MAGENTA);
        textView.setBackgroundColor(Color.BLACK);
    }
}
