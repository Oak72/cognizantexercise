# CognizantPracticalExam

The CognizantPracticalExam is a practical exam project developed as part of the Cognizant practical exam. It is an Android application that demonstrates various functionalities related to contact retrieval, encoding, and upload.

## Features

- **Contact Retrieval:** The application retrieves the contacts from the device's contact list and collects their information, such as name and phone number.

- **Base64 Encoding:** The application provides base64 encoding functionality to encode and decode strings.

- **Contact Upload:** The application uploads the retrieved contacts to a remote server using an HTTP POST request. The contacts are sent as a JSON array.

- **Application Package Name Upload:** Not used by default, but when the Base64Service is started, it retrieves the application package name and uploads it.

## Installation

1. Clone the repository to your local machine.

2. Open the project in Android Studio.

3. Build and run the application on an Android device or emulator.

## Usage

1. Launch the application on your Android device.

2. The main activity will display a text view with a message.

3. The application automatically starts the ContactRetrieverService, which retrieves the contacts from the device.

4. Once the contacts are retrieved, the ContactRetrieverService starts the UploadService to upload the contacts to the remote server.

5. A method is available to retrieve the application name and encrypt it using base64.

6. After retrieving the application name, the UploadService is leveraged to send it as a JSON array in the HTTP POST request.

7. All strings used in the project are encrypted, and the decryption is made available through the StringEncryptor class.

**Note:** I did not actually manage to test this as I'm currently unable to start the emulator on my personal computer. That being said, I'm fairly confident that this is working as intended due to the slight "adapted" TDD approach I had to take.
